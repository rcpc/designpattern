#include <iostream>
#include <vector>
#include <algorithm>

struct State
{
	std::string name;
	int value;
};

class Memento
{
public:
	Memento(const State& state = {"Undefined", 0}) : state(state)
	{
		// Empty
	}

	State getState() const
	{
		return state;
	}

	void setState(const State& other)
	{
		state.name = other.name;
		state.value = other.value;
	}

	friend bool operator == (const Memento& first, const Memento& second)
	{
		return first.getState().value == second.getState().value && first.getState().name == second.getState().name;
	}

private:
	State state;
};

class Life
{
public:
	void setMemento(const Memento& memento)
	{
		state = memento.getState();
	}

	Memento createMemento()
	{
		return Memento(state);
	}

	State getState()
	{
		return state;
	}

	friend std::ostream& operator << (std::ostream& os, const Life& life)
	{
		os << life.state.name << " " << life.state.value;
		return os;
	}

private:
	State state;
};



class TimeTraveller
{
public:
	void addMemento(const Memento& memento)
	{
		mementos.push_back(memento);
	}

	Memento takeMemento(const Memento& memento) const
	{
		auto myMemento = std::find(mementos.begin(), mementos.end(), memento);
		return *myMemento;
	}

	Memento takeMemento(int value) const
	{
		auto myMemento = std::find_if(mementos.begin(), mementos.end(), [&](const Memento& memento) -> bool { return memento.getState().value == value; });
		return *myMemento;
	}

private:
	std::vector<Memento> mementos;
};


void goForward(Life& myLife, TimeTraveller& timeTraveller)
{
	myLife.setMemento(timeTraveller.takeMemento(myLife.getState().value + 1));
}

void goBackward(Life& myLife, TimeTraveller& timeTraveller)
{
	myLife.setMemento(timeTraveller.takeMemento(myLife.getState().value - 1));
}

auto main() -> int
{
	Life myLife;

	TimeTraveller timeTraveller;
	myLife.setMemento({ {"You are 1 years old", 1 } });
	timeTraveller.addMemento(myLife.createMemento());

	myLife.setMemento({ {"You are 2 years old", 2 } });
	timeTraveller.addMemento(myLife.createMemento());

	myLife.setMemento({ {"You are 3 years old", 3 } });
	timeTraveller.addMemento(myLife.createMemento());

	int number;
	while (true)
	{
		std::cin >> number;
		switch (number)
		{
		case 1:
			goBackward(myLife, timeTraveller);
			std::cout << myLife << std::endl;
			break;
		case 2: 
			goForward(myLife, timeTraveller);
			std::cout << myLife << std::endl;
			break;
		default:
			break;
		}
	}



	// timeTraveller.addMemento(myLife.createMemento("You are 1 yeard old", 1));
	// timeTraveller.addMemento(myLife.createMemento("You are 2 yeard old", 2));
	// timeTraveller.addMemento(myLife.createMemento("You are 3 yeard old", 3));








	/*
	myLife.createMemento();
	timeTraveller.addMemento(myLife
	Memento myMemento;
	*/ 


	return 0;
}