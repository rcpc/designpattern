#pragma once
#include "IRound.h"

class Circle : public IRound
{
public:
	Circle() = default;
	~Circle() = default;

public:
	double getArea() override
	{
		return 0.0;
	}
};
