#pragma once
#include "IAngle.h"

class Square : public IAngle
{
public:
	Square()
	{
		std::cout << "Square created " << std::endl;
	}

	~Square() = default;

public:
	double getArea() override
	{
		return 0.0;
	}
};
