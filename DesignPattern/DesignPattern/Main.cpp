#include <iostream>
#include "FlatFactory.h"
#include "PerfectFactory.h"

// Abstract Factory.

// Shape,

/*
Abstract Factory from scratch

Demonstrate the Abstract Factory pattern with a simple program. 
The products are implementations of an abstract Shape class like Circle, Square, Ellipse, and Rectangle.

Define an abstract factory and two concrete factory classes, one responsible for the �perfect� shapes family and one for the �flattened� family. 

IShapeFactory // Abstract one

PerfectShapeFactory

FlattenedShapeFactory

Test your implementation by instantiating one of the two factories (the decision is made at runtime) and populating an array with the produced objects. 
Verify that they belong to the family you expect.

Discussion. "Think of constructors as factories that churn out objects". Here we are allocating the constructor responsibility to a factory object, and then using inheritance and virtual member functions to provide a "virtual constructor" capability. So there are two dimensions of decoupling occurring. The client uses the factory object instead of "new" to request instances; and, the client "hard-wires" the family, or class, of that factory only once, and throughout the remainder of the application only relies on the abstract base class.
*/


auto main() -> int
{
	FlatFactory* flatFactory = new FlatFactory();
	PerfectFactory* perfectFactory = new PerfectFactory();

	Square* a = dynamic_cast<Square*>(perfectFactory->getAngle());
	return 0;
}