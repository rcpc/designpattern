#pragma once

class IRound
{
public:
	IRound() = default;
	~IRound() = default;

public:
	virtual double getArea() = 0;
};
