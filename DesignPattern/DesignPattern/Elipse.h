#pragma once
#include "IRound.h"

class Elipse : public IRound
{
public:
	Elipse() = default;
	~Elipse() = default;

public:
	double getArea() override
	{
		return 0.0;
	}
};
