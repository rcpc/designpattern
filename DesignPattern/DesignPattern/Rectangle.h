#pragma once
#include "IAngle.h"

class Rectangle : public IAngle
{
public:
	Rectangle() = default;
	~Rectangle() = default;

public:
	double getArea() override
	{
		return 0.0;
	}
};
