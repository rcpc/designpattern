#pragma once
class IAngle
{
public:
	IAngle() = default;
	~IAngle() = default;
public:
	virtual double getArea() = 0;
};
