#pragma once
#include "IAngle.h"
#include "IRound.h"

class IFactory
{
public:
	IFactory() = default;
	~IFactory() = default;
public:
	virtual IAngle* getAngle() = 0;
	virtual IRound* getRound() = 0;
};
