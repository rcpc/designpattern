#pragma once
#include "IFactory.h"
#include "Elipse.h"
#include "Rectangle.h"

class FlatFactory : public IFactory
{
public:
	 FlatFactory() = default;
	~FlatFactory() = default;
public:
	IRound* getRound() override
	{
		return new Elipse();
	}

	IAngle* getAngle() override
	{
		return new Rectangle();
	}
};
