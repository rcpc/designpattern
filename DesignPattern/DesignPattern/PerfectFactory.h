#pragma once
#include "IFactory.h"
#include "Circle.h"
#include "Square.h"

class PerfectFactory : public IFactory
{
public:
	PerfectFactory() = default;
	~PerfectFactory() = default;

public:
	IRound* getRound() override
	{
		return new Circle();
	}

	IAngle* getAngle() override
	{
		return new Square();
	}

};
