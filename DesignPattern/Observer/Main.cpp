#include <vector>
#include <memory>
#include <iostream>

class IObserver
{
public:
	virtual ~IObserver() = default;
	virtual void update() = 0;
};


class Subject
{
public:
	Subject(int number)
		:mValue(number)
	{
		// Empty
	}

	void attach(std::shared_ptr<IObserver> observer)
	{
		_observers.push_back(observer);
	}

	void setState(int value)
	{
		mValue = value;
		for (auto& observer : _observers)
		{
			observer->update();
		}
	}

	int getState() const { return mValue; }
private:
	std::vector<std::shared_ptr<IObserver>> _observers;
	int mValue;
};

class DividerObserver : public IObserver
{
public:
	DividerObserver(Subject& subject) :
		_subject(subject)
	{}

	void update() override
	{
		std::cout << _subject.getState() / 2;
	}
private:
	Subject& _subject;
};

class ModuloObserver : public IObserver
{
public:
	ModuloObserver(Subject& subject) :
		mSubject(subject)
	{
		mSubject = subject;
	}

	void update() override
	{
		std::cout << mSubject.getState() % 5;
	}
private:
	Subject& mSubject;
};


int main()
{
	Subject subject(8);

	auto moduloObserver = std::make_shared<ModuloObserver>(subject);
	subject.attach(moduloObserver);
	subject.attach(std::make_shared<DividerObserver>(subject));

	subject.setState(15);
	return 0;
}